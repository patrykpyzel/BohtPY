class MongoClient:
    # member_ids = []

    def __init__(self, client):
        self._db = client.motor.zagadka
        self._better_members = {}

    def add_better_member(self, better_member):
        if better_member.id not in self._better_members:
            self._better_members[better_member.id] = better_member

    async def get_real_fake_by_inviter(self, inviter):
        guild = inviter.m.guild
        real_count = 0
        fake_count = 0
        cursor = self._db.members.find({"inviter": inviter.id})
        async for member in cursor:
            invited = guild.get_member(member["_id"])
            if (
                invited
                and invited.avatar
                and (invited.joined_at - invited.created_at) > timedelta(days=join_days)
            ):
                real_count += 1
            else:
                fake_count += 1
        return real_count, fake_count

    async def get_better_member(self, member, invite=None, on_member_join=False):
        # if member.id not in self.members
        better_member = self._better_members.get(member.id)
        # member_db = None
        if better_member:
            if on_member_join and better_member:
                better_member.update_on_member_join(invite)
        else:
            member_db = await self.get_member_db(member)
            if not member_db:
                await self.insert_member_db(member, invite)
                member_db = await self.get_member_db(member)

            better_member = BetterMember(self, member, member_db)
            # self._better_members[member.id] =
        # elif await self.get_member_db(member)
        # member_db = await self.get_member_db(member)
        # print(member_db)

        return better_member

    async def get_member_db(self, member):
        return await self._db.members.find_one({"_id": member.id})

    async def get_guild_by_id(self, guild_id):
        guild = await self.db_guilds.find_one({"_id": guild_id})
        return guild
        # print(guild)

    async def get_members(self):
        members = self._db.members.find()
        members_list = []
        for member in await members.to_list(length=100):
            members.append(member)
        return members_list

    async def update_invite_db(self, member_id, invite):
        self._db.members.update(
            {"_id": member_id},
            {
                "$set": {
                    "invite": invite.code if invite else None,
                    "joined_rec_at": datetime.now(),
                }
            },
        )

    async def insert_member_db(self, member, invite=None):
        now = datetime.now()
        score = {
            "Monday": 0,
            "Tuesday": 0,
            "Wednesday": 0,
            "Thursday": 0,
            "Friday": 0,
            "Saturday": 0,
            "Sunday": 0,
        }
        voice_channel = {"view_channel": True, "connect": True, "speak": True}
        voice_channel_members = {}

        permissions = {}

        member = {
            "_id": member.id,
            "invite": invite.code if invite else None,
            "inviter_rec": invite.inviter.id if invite and invite.inviter else None,
            "inviter": invite.inviter.id if invite and invite.inviter else None,
            "joined_rec_at": now,
            "joined_at": now,
            "score": score,
            "voice_channel": voice_channel,
            "voice_channel_members": voice_channel_members,
            "permissions": permissions,
            "usd": 0,
            "premium": 0,
            "premium_to": None,
        }
        return await self._db.members.insert_one(member)
        # async def do_insert():
        #
        #
        # await do_insert()

        # self.loop.run_until_complete(do_insert())

    async def insert_guild(self, guild_id, channel_id, code, inviter_id, language):
        guild = {
            "_id": guild_id,
            "channel_id": channel_id,
            "code": code,
            "inviter_id": inviter_id,
            "language": language,
        }
        await self._db.guilds.insert_one(guild)
