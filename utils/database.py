import logging
from collections import defaultdict, namedtuple
from datetime import datetime
from functools import wraps
from typing import Union

import aiosqlite
import hikari

# from hikari import User

logger = logging.getLogger("bot.DB")

DB_FILE = "data/boht.sqlite"

User = namedtuple("User", "id reputation reputation_date credits_date info credits")
UserPoints = namedtuple("UserPoints", "user_id points")
MemberAll = namedtuple("MemberAll", "id reputation credits score temp_score")


class EmptyQuery(Exception):
    pass


def first_arg_instance_to_instance_id(*instances):
    def dec(fn):
        @wraps(fn)
        def inner(id_, *args, **kwargs):
            if isinstance(id_, instances):
                id_ = id_.id
            return fn(id_, *args, **kwargs)

        return inner

    return dec


class ORM:
    _instances = {}
    python_columns_to_sqlite_columns = {
        "id": "userId",
        "reputation": "reputation",
        "reputation_date": "repDate",
        "credits_date": "creditsDate",
        "info": "userInfo",
        "credits_": "credits",
    }

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(ORM, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    @staticmethod
    @first_arg_instance_to_instance_id(hikari.Member, hikari.User)
    async def get_user(member_id: Union[int, hikari.Member, hikari.User]) -> User:
        async with aiosqlite.connect(DB_FILE) as conn:
            query = f"SELECT * FROM user WHERE userId={member_id}"

            async with conn.execute(query) as cr:
                async for row in cr:
                    return User(*row)
                else:
                    await ORM.set_user(member_id)
                    return await ORM.get_user(member_id)
                    # raise EmptyQuery('No User for given id')

    @staticmethod
    @first_arg_instance_to_instance_id(hikari.Member, hikari.User)
    async def set_user(member_id: Union[int, hikari.Member, hikari.User]) -> int:
        async with aiosqlite.connect(DB_FILE) as conn:
            query = (
                f"INSERT OR IGNORE INTO user (userId, reputation, repDate, creditsDate, userInfo, credits) "
                f"VALUES (?, ?, ?, ?, ?, ?)"
            )
            params = (member_id, 0, 0, 0, "", 0)
            await conn.execute(query, params)
            await conn.commit()
            return member_id

    @staticmethod
    def list_to_insert_str(lst):
        s = ""
        for elem in lst:
            s += ORM.python_columns_to_sqlite_columns[elem] + " = ?, "
        return s

    @staticmethod
    @first_arg_instance_to_instance_id(hikari.Member, hikari.User)
    async def update_user(
        member_id: Union[int, hikari.Member, hikari.User],
        columns: list,
        params: list,
    ):
        async with aiosqlite.connect(DB_FILE) as conn:
            query = f"UPDATE user SET {ORM.list_to_insert_str(columns)} WHERE userId = {member_id}"
            await conn.execute(query, params)
            await conn.commit()

    @staticmethod
    def datetime_diff_in_minutes(t1, t2):
        return int((t1 - t2) / 1000 / 60)

    @staticmethod
    def _add_rep_query(target_id: int, value: str) -> str:
        return f"UPDATE user SET reputation = reputation {value} 1 WHERE userId = {target_id}"

    @staticmethod
    def _update_db_column(
        target_id: int,
        table: str,
        where_column: str,
        column: str,
        value: int,
        sign: str = None,
    ):
        value = f"{column} {sign} {value}" if sign else value
        to_return = (
            f"UPDATE {table} SET {column} = {value} WHERE {where_column} = {target_id}"
        )
        return to_return

    @staticmethod
    async def get_member(member_id: int, guild_id: int):
        async with aiosqlite.connect(DB_FILE) as conn:
            query = (
                f"SELECT user.userId, reputation, credits, score, tempScore from user "
                f"LEFT JOIN guild_user "
                f"ON user.userId = guild_user.userId "
                f"WHERE user.userId={member_id} and guildId={guild_id}"
            )
            cursor = await conn.execute(query)
            row = await cursor.fetchone()
            # member = MemberAll(*row)
            return MemberAll(*row) if row else None

            # MemberAll = namedtuple('id reputation credits score temp_score')
        # return points

    @staticmethod
    async def add_rep(
        author_id: Union[int, hikari.Member, hikari.User],
        target_id: Union[int, hikari.Member, hikari.User],
        value: str = "+",
    ) -> (str, int, int):
        """Grants a reputation point to user.

        :param author_id: user(id) who gives reputation.
        :param target_id: user(id) who receives reputation.
        :param value: + or -
        :returns status code (str) of transaction, all Reputation points (int), minutes to next rep (int
            J: Joke
            A: Added reputation point
            M: Can add reputation
            N: Cannot add reputation
        """
        datetime_now = datetime.now()
        timestamp_now = int(datetime_now.timestamp() * 10**3)

        author = await ORM.get_user(author_id)
        target = (
            await ORM.get_user(target_id)
            if target_id and target_id != author_id
            else None
        )

        datetime_diff_in_minutes = ORM.datetime_diff_in_minutes(
            timestamp_now, author.reputation_date
        )

        if datetime_diff_in_minutes > 60 * 6:
            if target_id == author_id:
                return "J", author.reputation, 0
            elif target:
                async with aiosqlite.connect(DB_FILE) as conn:
                    await conn.execute(
                        ORM._update_db_column(
                            target.id,
                            table="user",
                            where_column="userId",
                            column="reputation",
                            value=1,
                            sign=value,
                        )
                    )
                    await conn.execute(
                        ORM._update_db_column(
                            author.id,
                            table="user",
                            where_column="userId",
                            column="repDate",
                            value=timestamp_now,
                        )
                    )
                    await conn.commit()
                    reps = (
                        target.reputation + 1 if value == "+" else target.reputation - 1
                    )
                    return "A", reps, 0
            else:
                return "M", author.reputation, 0
        else:
            return "N", author.reputation, 6 * 60 - datetime_diff_in_minutes

    @staticmethod
    async def get_top(guild_id: int, version: str) -> [UserPoints]:
        score = "tempScore" if version == "month" else "score"
        async with aiosqlite.connect(DB_FILE) as conn:
            query = (
                f"SELECT userId, {score} FROM guild_user WHERE guildId={guild_id} "
                f"ORDER BY {score} DESC LIMIT 1000"
            )
            points = []
            async with conn.execute(query) as cr:
                async for row in cr:
                    points.append(UserPoints(int(row[0]), int(row[1])))
        return points

    @staticmethod
    async def add_points(guild_id: int, member_id: int):
        await ORM.set_user(member_id)
        async with aiosqlite.connect(DB_FILE) as conn:
            query = (
                f"INSERT INTO guild_user(guildId, userId, score, tempScore) "
                f"VALUES({guild_id}, {member_id}, {0}, {0}) "
                f"ON CONFLICT(guildId, userId) DO UPDATE SET "
                f"score = score + 1, tempscore = tempscore + 1"
            )
            await conn.execute(query)
            await conn.commit()

    @staticmethod
    async def set_guild(guild_id: int) -> None:
        async with aiosqlite.connect(DB_FILE) as conn:
            query = f"INSERT OR IGNORE INTO guild (guildId, prefix, language) VALUES (?, ?, ?)"
            params = (guild_id, ".", "PL")
            await conn.execute(query, params)
            await conn.commit()
            # return member_id

    @staticmethod
    async def reset_points(guild_id: int, member_id: int = None) -> None:
        async with aiosqlite.connect(DB_FILE) as conn:
            member_reset = f" and userId = {member_id}" if member_id else ""
            if member_id:
                query = (
                    f"UPDATE guild_user "
                    f"SET score = score - tempScore, tempScore = 0 "
                    f"WHERE guildId = {guild_id} {member_reset}"
                )
            else:
                query = (
                    f"UPDATE guild_user "
                    f"SET score = score, tempScore = 0 "
                    f"WHERE guildId = {guild_id} {member_reset}"
                )
            await conn.execute(query)
            await conn.commit()

    @staticmethod
    async def initialize():
        async with aiosqlite.connect(DB_FILE) as conn:
            await conn.execute(
                """
                create table if not exists user
                (
                    userId TEXT not null
                        primary key,
                    reputation INTEGE
                    repDate INTEGER,
                    creditsDate INTEGER,
                    userInfo TEXT,
                    credits INTEGER
                )
                without rowid;
            """
            )
            await conn.execute(
                """
                create table if not exists guild
                (
                    guildId TEXT not null
                        primary key,
                    prefix TEXT,
                    language TEXT
                )
                without rowid;
            """
            )
            await conn.execute(
                """
                create table if not exists role (
                    roleId TEXT not null,
                    expireDate INTEGER,
                    role TEXT,
                    guildId TEXT not null
                        references guild,
                    primary key (roleId, guildId)
                )
                without rowid;
            """
            )
            await conn.execute(
                """
                create table if not exists guild_user
                (
                    score INTEGER,
                    tempScore INTEGER,
                    userGuildInfo TEXT,
                    userId TEXT not null
                        references user,
                    guildId TEXT not null
                        references guild,
                    primary key (userId, guildId)
                )
                without rowid;
            """
            )
            await conn.execute(
                """
                create table if not exists message
                (
                    messageId TEXT not null,
                    message TEXT,
                    active INTEGER,
                    guildId TEXT not null
                        references guild,
                    primary key (messageId, guildId)
                )
                without rowid;
            """
            )
            await conn.execute(
                """
                create table if not exists channel
                (
                    channelId TEXT not null,
                    channel TEXT,
                    active INTEGER,
                    guildId TEXT not null
                        references guild,
                    primary key (channelId, guildId)
                )
                without rowid;
            """
            )

    @staticmethod
    async def close_connection():
        async with aiosqlite.connect(DB_FILE) as conn:
            await conn.close()
