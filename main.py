import asyncio
import datetime
import logging
import os
from abc import ABC
from typing import Callable, Iterable, Union
import traceback
import aiohttp
import aiosqlite
import dotenv
import hikari
import lightbulb
import miru
from lightbulb.ext import tasks

from utils import database as db

dotenv.load_dotenv()

class Bot(lightbulb.BotApp, ABC):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.dev_logs: hikari.GuildTextChannel = 985671174795182120
        self.dev_join_leave_logs: hikari.GuildTextChannel = 988093216274382858
        self.color = 0x3B9DFF
        self.invite_bot_url = (
            "https://discord.com/api/oauth2/authorize?"
            "client_id=489377322042916885&"
            "permissions=8&"
            "scope=bot%20applications.commands"
        )
        self.invite_zagadka = "https://discord.gg/tUzZDd32jZ"
        self.zagadka_guild = 960665311701528596
        self.nsfw_roles = {
            960665311953174558,
        }
        self.allowed_role = 960665311730868241
        self.last_message_time = datetime.datetime.now()

    @classmethod
    def create(
        cls, token: str, *, prefix: Union[Callable, Iterable, str], **kwargs
    ) -> "Bot":
        return cls(
            token=token,
            prefix=prefix,
            intents=hikari.Intents.ALL,
            insensitive_commands=True,
            ignore_bots=True,
            **kwargs,
        )

    def run(self, **kwargs) -> None:
        self.event_manager.subscribe(hikari.StartingEvent, self.on_starting)
        self.event_manager.subscribe(hikari.StartedEvent, self.on_started)
        self.event_manager.subscribe(hikari.StoppingEvent, self.on_stopping)
        self.event_manager.subscribe(hikari.ExceptionEvent, self.on_exception)

        super().run(
            activity=hikari.Activity(
                name=f".help\n"
                f"Jeśli komendy /slash nie działają spróbuj dodać bota na serwer ponownie z uprawnieniami.",
                type=hikari.ActivityType.PLAYING,
            ),
            **kwargs,
        )

    async def on_starting(self, event: hikari.StartingEvent) -> None:
        """Load extensions when bot is starting."""
        logging.info("Connecting to database...")

        self.d.db = db.ORM()
        self.d.aio_session = aiohttp.ClientSession()

        logging.info("Loading extensions...")
        self.load_extensions_from("./extensions/", must_exist=True, recursive=True)
        logging.info("Done loading extensions.")
        self.miru = miru.Client(self)

    async def on_started(self, event: hikari.StartedEvent) -> None:
        """Notify dev-logs."""
        self.dev_logs = await self.rest.fetch_channel(self.dev_logs)
        self.dev_join_leave_logs = await self.rest.fetch_channel(
            self.dev_join_leave_logs
        )
        self.zagadka_guild = await self.rest.fetch_guild(self.zagadka_guild)
        await self.refresh_zagadka()
        await self.dev_logs.send(f"Bot online!")
        logging.info("Loading tasks...")
        tasks.load(self)
        logging.info("Bot is ready.")

    async def refresh_zagadka(self):
        self.d.zagadka_members = self.zagadka_guild.get_members()

    async def on_stopping(self, event: hikari.StoppingEvent) -> None:
        """Disconnect Apps."""
        await self.d.aio_session.close()
        await self.dev_logs.send(f"Bot offline!")

    async def on_exception(self, event: hikari.ExceptionEvent) -> None:
        """Handle unexpected exceptions."""
        logging.error(f"Unexpected error: {event.exception}")
        await self.dev_logs.send(f"An error occurred: {event.exception}")

    @tasks.task(s=5)
    async def print_every_5_seconds(self):
        print("Task called")

    @staticmethod
    async def get_mention(ctx: lightbulb.Context) -> Union[hikari.Member, hikari.User, None]:
        try:
            if isinstance(ctx, lightbulb.PrefixContext):
                if ctx.event.message.user_mentions:
                    return next(iter(ctx.event.message.user_mentions.values()), None)
                if ctx.event.message.referenced_message:
                    return ctx.event.message.referenced_message.author
            return None
        except Exception as e:
            logging.error(f"Error in get_mention: {str(e)}")
            logging.error(traceback.format_exc())
            return None

    @classmethod
    async def get_target(
        cls, ctx: lightbulb.Context, trg: Union[hikari.Member, hikari.User, int, str] = None
    ) -> Union[hikari.Member, hikari.User, None]:
        """Fetches a Member if available, otherwise returns a User."""
        try:
            logging.info(f"get_target called with trg: {trg}")

            if trg is None:
                if isinstance(ctx, lightbulb.SlashContext) and hasattr(ctx.options, 'target'):
                    trg = ctx.options.target
                else:
                    return ctx.member or ctx.author

            if isinstance(trg, hikari.Member):
                return trg

            if isinstance(trg, (int, str)):
                try:
                    user_id = int(trg)
                except ValueError:
                    logging.info(f"Invalid user ID: {trg}")
                    return None

                if ctx.guild_id:
                    try:
                        return await ctx.bot.rest.fetch_member(ctx.guild_id, user_id)
                    except hikari.NotFoundError:
                        logging.info(f"Member not found in guild, fetching user: {user_id}")
                
                try:
                    return await ctx.bot.rest.fetch_user(user_id)
                except hikari.NotFoundError:
                    logging.info(f"User not found: {user_id}")
                    return None

            if isinstance(trg, hikari.User) and ctx.guild_id:
                try:
                    return await ctx.bot.rest.fetch_member(ctx.guild_id, trg.id)
                except hikari.NotFoundError:
                    logging.info(f"Member not found in guild, returning User: {trg.id}")
                    return trg

            mention = await cls.get_mention(ctx)
            if mention:
                if ctx.guild_id:
                    try:
                        return await ctx.bot.rest.fetch_member(ctx.guild_id, mention.id)
                    except hikari.NotFoundError:
                        logging.info(f"Mentioned member not found in guild: {mention.id}")
                return mention

            return ctx.member or ctx.author

        except Exception as e:
            logging.error(f"Error in get_target: {str(e)}")
            logging.error(traceback.format_exc())
            return None



    @staticmethod
    def number_to_time(number: int = 0, time: str = "") -> str:
        if number == 0:
            return ""
        if number == 1:
            return str(number) + " " + time + "ę"
        elif number % 10 in {1, 5, 6, 7, 8, 9, 0} or number in {12, 13, 14}:
            return str(number) + " " + time
        else:
            return str(number) + " " + time + "y"

    @classmethod
    def time_to_text(cls, hours: int = 0, minutes: int = 0, seconds: int = 0) -> str:
        return ", ".join(
            cls.number_to_time(number, time)
            for number, time in zip(
                (hours, minutes, seconds), ("godzin", "minut", "sekund")
            )
            if cls.number_to_time(number, time)
        )

    @staticmethod
    def embed_perms(ctx):
        bot_member = (
            ctx.bot.cache.get_member(ctx.guild_id, ctx.bot.get_me().id)
            if ctx.guild_id
            else None
        )
        if (
            bot_member
            and hikari.Permissions.EMBED_LINKS
            in lightbulb.utils.permissions.permissions_in(ctx.get_channel(), bot_member)
        ):
            return True


bot = Bot(
    token=os.environ["BOT_TOKEN"],
    prefix=os.environ["BOT_PREFIX"],
    banner=None,
    case_insensitive_prefix_commands=True,
    intents=hikari.Intents.ALL,
    default_enabled_guilds=(),
)

if __name__ == "__main__":
    if os.name != "nt":
        import uvloop
        uvloop.install()
    bot.run()
