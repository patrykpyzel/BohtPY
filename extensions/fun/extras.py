from random import choice

import hikari
import lightbulb
from lightbulb.ext import filament

ext_plugin = lightbulb.Plugin("extras", "random commands", include_datastore=True)


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("why", "Askin the real question here", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand)
async def whytho(ctx: lightbulb.Context):
    async with ctx.bot.d.aio_session.get("https://nekos.life/api/v2/why") as why:
        data = await why.json()
        result = data.get("why")
        embed = hikari.Embed(description=result, color=0x8253C3)
        await ctx.respond(embed=embed, reply=True)


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("name", "A random name generator", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand)
async def namedeez(ctx: lightbulb.Context):
    async with ctx.bot.d.aio_session.get("https://nekos.life/api/v2/name") as name:
        data = await name.json()
        result = data.get("name")
        embed = hikari.Embed(description=result, color=0x8253C3)
        await ctx.respond(embed=embed, reply=True)


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("rickroll", "You have been rickrolled!", aliases=["rr"])
@lightbulb.implements(lightbulb.PrefixCommand)
async def rickroll(ctx: lightbulb.Context):
    rick = hikari.Embed()
    rick.set_image(
        "https://i.kym-cdn.com/photos/images/original/000/041/494/1241026091_youve_been_rickrolled.gif"
    )
    await ctx.respond(embed=rick, reply=True)


ext_plugin.d.ps = {
    "psgood": [
        "Tak",
        "To jest pewne",
        "Moim zdaniem tak",
        "Na pewno",
        "Tak, absolutnie" "Możesz na to liczyć" "Bez wątpienia" "Bardzo prawdopodobne ",
    ],
    "psbad": [
        "Nie",
        "Mało prawdopodobne",
        "Chyba w snach",
        "Nie licz na to",
        "Niemożliwe",
        "Bez szans",
    ],
}


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "question",
    "Jakie Pytanie chcesz zadać?",
    str,
    required=True,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.command("8ball", "Zadaj pytanie 8ball")
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def eightball(ctx: lightbulb.Context, question: str):
    choices = choice(choice(list(ext_plugin.d.ps.values())))
    if choices in ext_plugin.d.ps["psbad"]:
        color = hikari.Color(0xFF0000)
    else:
        color = hikari.Color(0x26D934)
    eightball = hikari.Embed(color=color)
    eightball.add_field(name="Pytanie:", value=question.capitalize(), inline=False)
    eightball.add_field(name="Odpowiedź:", value=f"{choices}.")
    eightball.set_author(name="Kula 8-Ball")
    eightball.set_footer(
        f"Zapytanie od: {ctx.author.username}", icon=ctx.author.avatar_url
    )
    eightball.set_thumbnail("https://i.imgur.com/Q9dxpTz.png")
    await ctx.respond(embed=eightball, reply=True)


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "text",
    "Dla kogo respect?",
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.command("f", "Press F")
@lightbulb.implements(lightbulb.PrefixCommand)
@filament.utils.pass_options
async def respect(ctx: lightbulb.Context, text: str):
    hearts = ["❤", "💛", "💚", "💙", "💜", "♥"]
    reason = f"Dla **{text}** " if text else ""
    await ctx.respond(
        f"**{ctx.author.username}** Oddał respect {reason}{choice(hearts)}", reply=True
    )


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "Powód bana",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("user", "Kogo chcesz zbanować?", hikari.Member, required=True)
@lightbulb.command("fakeban", "Zbanuj użytkownika.", aliases=["fban"])
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def fakeban(ctx: lightbulb.Context, user: hikari.Member, reason: str):
    await ctx.respond(f"Zbanowałem `{user}`!", reply=True)


@ext_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "the reason for muting the member",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("user", "the user you want to mute", hikari.Member, required=True)
@lightbulb.command("fakemute", "mute a member (Or is it?)", aliases=["fmute"])
@lightbulb.implements(lightbulb.PrefixCommand)
@filament.utils.pass_options
async def fakemute(ctx: lightbulb.Context, user: hikari.Member, reason: str):
    await ctx.respond(f"Zmutowałem {user.mention}!", reply=True)


def load(bot):
    bot.add_plugin(ext_plugin)


def unload(bot):
    bot.remove_plugin(ext_plugin)
