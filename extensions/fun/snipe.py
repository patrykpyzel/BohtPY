from datetime import datetime

import hikari
import lightbulb
from lightbulb.ext import tasks

sniper = lightbulb.Plugin("snipe", "snippin yo ass", include_datastore=True)

sniper.d.delsniped = {}
sniper.d.editsniped = {}


@sniper.listener(hikari.GuildMessageDeleteEvent)
async def on_guild_message_delete(event: hikari.GuildMessageDeleteEvent):
    try:
        msg = event.old_message
        if not msg.author.is_bot:
            srvid = msg.guild_id
            chid = msg.channel_id
            auth_name = msg.author.username
            auth_mention = msg.author.mention
            content = msg.content
            if sniper.app.check_links_and_img(content):
                return
            try:
                attach = msg.attachments[0]
                attachment_name = attach.filename
                attachment_file = attach.url or attach.proxy_url
            except (IndexError, KeyError):
                attachment_name = None
                attachment_file = None

            sniper.d.delsniped.update(
                {
                    srvid: {
                        chid: {
                            "Author": msg.author,
                            "Sender": auth_name,
                            "Mention": auth_mention,
                            "Content": content,
                            "Attachment": attachment_file,
                            "Filename": attachment_name,
                        }
                    }
                }
            )
    except:
        pass


@sniper.listener(hikari.GuildMessageUpdateEvent)
async def on_guild_message_edit(event: hikari.GuildMessageUpdateEvent):
    try:
        new_msg = event.message
        old_msg = event.old_message
        if not old_msg.author.is_bot:
            srvid = new_msg.guild_id
            chid = new_msg.channel_id
            auth_name = new_msg.author.username
            auth_mention = new_msg.author.mention
            old_message = old_msg.content
            new_message = new_msg.content
            if sniper.app.check_links_and_img(old_message):
                return
            if sniper.app.check_links_and_img(new_message):
                return

            sniper.d.editsniped.update(
                {
                    srvid: {
                        chid: {
                            "Author": new_msg.author,
                            "Sender": auth_name,
                            "Mention": auth_mention,
                            "Before": old_message,
                            "After": new_message,
                        }
                    }
                }
            )
    except:
        pass


@sniper.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command(
    "snipe", "Sprawdź ostatnio usuniętą wiadomość ;)", aliases=["sn", "delsnipe"]
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def deletesnipe(ctx: lightbulb.Context) -> None:
    try:
        author = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Author"]
        name = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Sender"]
        author_mention = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Mention"]
        msg = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Content"]
        attachment = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Attachment"]

        if isinstance(ctx, lightbulb.PrefixContext):
            await ctx.event.message.delete()

        emb = hikari.Embed(
            description=msg or "Pusta wiadomość!", timestamp=datetime.now().astimezone()
        )
        emb.set_author(name="Co my tu mamy?", icon=author.avatar_url)
        emb.add_field(name="Autor:", value=f"{name} ({author_mention})", inline=False)
        emb.set_footer(
            f".snipe przez: {ctx.author.username}", icon=ctx.author.avatar_url
        )
        if attachment:
            filename = sniper.d.delsniped[ctx.guild_id][ctx.channel_id]["Filename"]
            emb.add_field(name="Attachments", value=f"[{filename}]({attachment})")
            if (
                str(filename).endswith(".png")
                or str(filename).endswith(".gif")
                or str(name).endswith(".jpg")
                or str(name).endswith(".jpeg")
            ):
                emb.set_image(attachment)
        await ctx.respond(embed=emb)
        del sniper.d.delsniped[ctx.guild_id][ctx.channel_id]
    except (KeyError, IndexError):
        await ctx.respond(
            embed=hikari.Embed(description="⚠ Nie ma nowych wiadomości!"),
            delete_after=3,
        )


@sniper.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command(
    "snipedit", "Sprawdź ostatnią edytowaną wiadomość ;)", aliases=["esnipe"]
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def editsnipe(ctx: lightbulb.Context) -> None:
    try:
        author = sniper.d.editsniped[ctx.guild_id][ctx.channel_id]["Author"]
        name = sniper.d.editsniped[ctx.guild_id][ctx.channel_id]["Sender"]
        author_mention = sniper.d.editsniped[ctx.guild_id][ctx.channel_id]["Mention"]
        before = sniper.d.editsniped[ctx.guild_id][ctx.channel_id]["Before"]
        after = sniper.d.editsniped[ctx.guild_id][ctx.channel_id]["After"]
        if isinstance(ctx, lightbulb.PrefixContext):
            await ctx.event.message.delete()

        emb = hikari.Embed(timestamp=datetime.now().astimezone())
        emb.set_author(name="Co to za edycja?", icon=author.avatar_url)
        emb.add_field(name="Autor:", value=f"{name} ({author_mention})", inline=False)
        emb.add_field(name="Before:", value=before)
        emb.add_field(name="After:", value=after)
        emb.set_footer(
            f".snipe przez: {ctx.author.username}", icon=ctx.author.avatar_url
        )
        await ctx.respond(embed=emb)
        del sniper.d.editsniped[ctx.guild_id][ctx.channel_id]
    except (KeyError, IndexError):
        await ctx.respond(
            embed=hikari.Embed(description="⚠ Nie ma nowych wiadomości!"),
            delete_after=3,
        )


@tasks.task(h=1, auto_start=True)
async def clear_sniper():
    sniper.d.editsniped.clear()
    sniper.d.delsniped.clear()


def load(bot) -> None:
    bot.add_plugin(sniper)


def unload(bot) -> None:
    sniper.d.clear()
    bot.remove_plugin(sniper)
