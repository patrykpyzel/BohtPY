# import time
from collections import namedtuple
from datetime import datetime, timedelta
from itertools import chain
import logging
import traceback

import hikari
import lightbulb
from lightbulb.ext import filament
from lightbulb.utils import nav, pag

User = namedtuple("User", "id points name")

top_plugin = lightbulb.Plugin("Top")


async def top_factory(version, ctx):
    try:
        guild = ctx.get_guild()
        db = ctx.bot.d.db
        guild_members = guild.get_members()

        users_points = await db.get_top(guild.id, version)
        users = []
        leavers = False
        try:
            leavers = ctx.options.leavers
        except AttributeError:
            pass
        for member in users_points:
            if int(member.user_id) in guild_members:
                users.append(User(*member, guild_members.get(member.user_id).display_name))
            elif leavers:
                users.append(User(*member, str(member.user_id)))

        bot_member = (
            ctx.bot.cache.get_member(ctx.guild_id, ctx.bot.get_me())
            if ctx.guild_id
            else None
        )
        if (
            bot_member
            and hikari.Permissions.EMBED_LINKS
            in lightbulb.utils.permissions.permissions_in(ctx.get_channel(), bot_member)
        ):
            lst = pag.EmbedPaginator(max_lines=11, prefix="```glsl\n", suffix="```")

            @lst.embed_factory()
            def build_embed(page_index, page_content):
                emb = hikari.Embed(
                    title=f"Top {version} - strona {page_index}",
                    description=page_content,
                    color=ctx.bot.color,
                )
                if version == "month":
                    emb.set_footer(".tmr resetuje ranking czasowy!")
                return emb

            add_0 = set(chain(range(1, 10), range(91, 100), range(990, 1000)))
            first = 0
            for n, user in enumerate(users, start=1):
                if n % 10 == 1:
                    first = 0
                position_and_points = f"{' ' if n in add_0 else ''}{str(n)}. {user.points}"
                just = first - len(position_and_points)
                name = " - #" + user.name
                lst.add_line(
                    f'{position_and_points}{" " * (first - len(position_and_points))}{name.ljust(just)}'
                )
                if n % 10 == 1:
                    first = len(position_and_points)

            navigator = nav.ButtonNavigator(lst.build_pages())
            await navigator.run(ctx)

        else:
            add_0 = set(chain(range(1, 10), range(91, 100), range(990, 1000)))
            lst = ["```glsl"]
            for n, user in enumerate(users, start=1):
                if n % 20 == 1:
                    first = 0
                position_and_points = f"{' ' if n in add_0 else ''}{str(n)}. {user.points}"
                just = first - len(position_and_points)
                name = " - #" + user.name
                lst.append(
                    f'{position_and_points}{" " * (first - len(position_and_points))}{name.ljust(just)}'
                )
                if n % 20 == 1:
                    first = len(position_and_points)
                elif n % 20 == 0:
                    break
            lst.append("```")
            return await ctx.respond("\n".join(lst))
    except Exception as e:
        logging.error(f"Error in top_factory: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@top_plugin.command
@lightbulb.command(
    "top",
    "Topka aktywności na serwerze.",
    aliases=["t", "rank", "leaderboard", "tm", "topmonth"],
)
@lightbulb.implements(lightbulb.SlashCommandGroup, lightbulb.PrefixCommandGroup)
async def top_group(ctx: lightbulb.Context) -> None:
    try:
        if ctx.invoked_with in {"t", "top", "topall"}:
            await top_factory("all", ctx)
        else:
            await top_factory("month", ctx)
    except Exception as e:
        logging.error(f"Error in top_group: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@top_group.child
@lightbulb.option(
    "leavers",
    "Czy liczyć konta nieobecne na serwerze?",
    bool,
    required=False,
    default=False,
)
@lightbulb.command("all", "Topka aktywności na serwerze.", aliases=["t", "a"])
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def top_all(ctx: lightbulb.Context) -> None:
    await top_factory("all", ctx)


@top_group.child
@lightbulb.option(
    "leavers",
    "Czy liczyć konta nieobecne na serwerze?",
    bool,
    required=False,
    default=False,
)
@lightbulb.command("month", "Miesięczna topka aktywności", aliases=["tm", "m"])
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def top_month(ctx: lightbulb.Context) -> None:
    await top_factory("month", ctx)


@top_plugin.command()
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.command("tmr", "Reset miesięcznego rankingu")
@lightbulb.implements(lightbulb.PrefixCommand)
async def top_month_reset(ctx: lightbulb.Context) -> None:
    try:
        await ctx.app.d.db.reset_points(ctx.guild_id)
        await ctx.respond("Zresetowałem!", reply=True)
    except Exception as e:
        logging.error(f"Error in top_month_reset: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas resetowania punktów. Spróbuj ponownie później.", reply=True)


@top_plugin.command()
@lightbulb.add_checks(lightbulb.has_guild_permissions(hikari.Permissions.ADMINISTRATOR))
@lightbulb.option(
    "target", "Komu zresetować miesięczny ranking?", hikari.User, required=True
)
@lightbulb.command("resetpoints", "Reset miesięcznego rankingu użytkownika")
@lightbulb.implements(lightbulb.PrefixCommand)
@filament.utils.pass_options
async def top_month_reset_member(ctx: lightbulb.Context, target: hikari.User) -> None:
    try:
        if target:
            await ctx.app.d.db.reset_points(ctx.guild_id, target.id)
            await ctx.respond("Zresetowałem!", reply=True)
        else:
            await ctx.respond("Nie znalazłem użytkownika!", reply=True)
    except lightbulb.errors.MissingRequiredPermission:
        await ctx.respond("Brak permisji!", reply=True)
    except Exception as e:
        logging.error(f"Error in top_month_reset_member: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas resetowania punktów. Spróbuj ponownie później.", reply=True)


def load(bot: lightbulb.BotApp) -> None:
    bot.add_plugin(top_plugin)
