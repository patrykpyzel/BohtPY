import asyncio
import os

import hikari
import lightbulb
import requests

fun_plugin = lightbulb.Plugin("FunPlugin")


@fun_plugin.command
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("text", "Czego chcesz sie dowiedzieć?", str, required=True)
@lightbulb.command("chat", "Spytaj mnie o coś!")
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def chat(ctx: lightbulb.Context, text: str):
    # Make a request to the ChatGPT API
    api_key = os.environ["openAI_key"]
    endpoint = "https://api.openai.com/v1/chatgpt/messages"
    headers = {"Content-Type": "application/json", "Authorization": f"Bearer {api_key}"}
    data = {
        "model": "chatgpt",
        "prompt": text,
        "max_tokens": 256,
        "stop": "\n",
        "temperature": 0.5,
    }
    response = requests.post(endpoint, headers=headers, json=data)
    response_text = response.json()["choices"][0]["text"]

    await ctx.send(response_text)


@fun_plugin.command
@lightbulb.command("fun", "Wszystkie zabawne komendy, których potrzebujesz")
@lightbulb.implements(lightbulb.SlashCommandGroup, lightbulb.PrefixCommandGroup)
async def fun_group(ctx: lightbulb.Context) -> None:
    pass  # as slash commands cannot have their top-level command ran, we simply pass here


@fun_group.child
@lightbulb.command("meme", "Wylosuj mema")
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def meme_subcommand(ctx: lightbulb.Context) -> None:
    async with ctx.bot.d.aio_session.get(
        "https://meme-api.herokuapp.com/gimme"
    ) as response:
        res = await response.json()

        if response.ok and res["nsfw"] is not True:
            link = res["postLink"]
            title = res["title"]
            img_url = res["url"]

            embed = hikari.Embed(colour=0x3B9DFF)
            embed.set_author(name=title, url=link)
            embed.set_image(img_url)

            await ctx.respond(embed)

        else:
            await ctx.respond(
                "Nie mogłem znaleźć mema :c",
                flags=hikari.MessageFlag.EPHEMERAL,
                reply=True,
            )


ANIMALS = {
    "Dog": "🐶",
    "Cat": "🐱",
    "Panda": "🐼",
    "Fox": "🦊",
    "Red Panda": "🐼",
    "Koala": "🐨",
    "Bird": "🐦",
    "Racoon": "🦝",
    "Kangaroo": "🦘",
}


@fun_group.child
@lightbulb.command("animal", "Dostaniesz fakt i obrazek zwierzaka :3")
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def animal_subcommand(ctx: lightbulb.Context) -> None:
    select_menu = (
        ctx.bot.rest.build_action_row()
        .add_select_menu("animal_select")
        .set_placeholder("Wybierz zwierzaka.")
    )

    for name, emoji in ANIMALS.items():
        select_menu.add_option(
            name,  # the label, which users see
            name.lower().replace(" ", "_"),  # the value, which is used by us later
        ).set_emoji(emoji).add_to_menu()

    resp = await ctx.respond(
        "Wybierz zwierzaka z menu :3",
        component=select_menu.add_to_container(),
    )
    msg = await resp.message()

    try:
        event = await ctx.bot.wait_for(
            hikari.InteractionCreateEvent,
            timeout=60,
            predicate=lambda e: isinstance(e.interaction, hikari.ComponentInteraction)
            and e.interaction.user.id == ctx.author.id
            and e.interaction.message.id == msg.id
            and e.interaction.component_type == hikari.ComponentType.SELECT_MENU,
        )
    except asyncio.TimeoutError:
        await msg.edit("Koniec czasu :c", components=[])
    else:
        animal = event.interaction.values[0]
        async with ctx.bot.d.aio_session.get(
            f"https://some-random-api.ml/animal/{animal}"
        ) as res:
            if res.ok:
                res = await res.json()
                embed = hikari.Embed(description=res["fact"], colour=0x3B9DFF)
                embed.set_image(res["image"])

                animal = animal.replace("_", " ")

                await msg.edit(
                    f"Oto {animal} dla Ciebie :3", embed=embed, components=[]
                )
            else:
                await msg.edit(f"API zwróciło {res.status} status :c", components=[])


def load(bot: lightbulb.BotApp) -> None:
    bot.add_plugin(fun_plugin)
