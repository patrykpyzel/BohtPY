from collections import namedtuple
from datetime import datetime, timedelta, timezone
import hikari
import lightbulb
import logging
import traceback

User = namedtuple("User", "id reputation reputation_date credits_date info credits")

rep_plugin = lightbulb.Plugin("Rep")

async def rep_factory(version, ctx):
    logging.info(f"Starting rep_factory with version '{version}' and context type {type(ctx)}")
    
    try:
        mention = ctx.options.target if hasattr(ctx.options, 'target') else await ctx.app.get_mention(ctx)
        logging.info(f"Mention after get_mention: {mention}")
        
        target_id = mention.id if mention else None
        logging.info(f"Final Target ID: {target_id}")
        
        status, reps, time = await ctx.bot.d.db.add_rep(ctx.author.id, target_id, version)
        logging.info(f"Add rep result: status='{status}', reps={reps}, time={time}")

        if status == "J":
            if version == "+":
                return await ctx.respond(
                    f"Stan Twojej reputacji: **{reps}**, "
                    f"możesz ją przydzielić komuś **fajnemu**.",
                    reply=True,
                )
            else:
                return await ctx.respond(
                    f"Stan Twojej reputacji: **{reps}**, "
                    f"szoruje ona po dnie, nie pogrążaj się więcej...",
                    reply=True,
                )
        elif status == "A":
            return await ctx.respond(
                f"{version}1 punkt reputacji dla <@{target_id}>, " f"razem: **{reps}**.",
                reply=True,
            )
        elif status == "M":
            return await ctx.respond(
                f"Stan Twojej reputacji: **{reps}**, " f"możesz już ją przydzielić.",
                reply=True,
            )
        elif status == "N":
            if time <= 60:
                now = datetime.now(timezone.utc)
                then = now + timedelta(minutes=time + 1)
                txt = f"<t:{int(then.timestamp())}:R>"
            else:
                txt = f'za {ctx.app.number_to_time(time, "minut")}'
            return await ctx.respond(
                f"Stan Twojej reputacji: **{reps}**, " f"możesz ją przydzielić {txt}.",
                reply=True,
            )
    except Exception as e:
        logging.error(f"Error in rep_factory: {str(e)}")
        logging.error(traceback.format_exc())
        return await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@rep_plugin.command
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("rep", "Przydziel reputację", aliases=["r", "reputation"])
@lightbulb.implements(lightbulb.SlashCommandGroup, lightbulb.PrefixCommandGroup)
async def rep_group(ctx: lightbulb.Context) -> None:
    try:
        await rep_factory("+", ctx)
    except Exception as e:
        logging.error(f"Error in rep_group: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@rep_group.child
@lightbulb.option("target", "Komu dajesz repa?", hikari.User, required=False)
@lightbulb.command("plus", "Daj +1 reputacji.", aliases=["+"])
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def rep_plus(ctx: lightbulb.Context) -> None:
    try:
        await rep_factory("+", ctx)
    except Exception as e:
        logging.error(f"Error in rep_plus: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@rep_group.child
@lightbulb.option("target", "Komu odejmujesz repa?", hikari.User, required=False)
@lightbulb.command("minus", "Daj -1 reputacji.", aliases=["-"])
@lightbulb.implements(lightbulb.SlashSubCommand, lightbulb.PrefixSubCommand)
async def rep_minus(ctx: lightbulb.Context) -> None:
    try:
        await rep_factory("-", ctx)
    except Exception as e:
        logging.error(f"Error in rep_minus: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

def load(bot: lightbulb.BotApp) -> None:
    bot.add_plugin(rep_plugin)