import hikari
import lightbulb
from lightbulb.ext import filament
from yarl import URL
import logging
import traceback
import random

img_plugin = lightbulb.Plugin("images", "Images manipulation related command")

cry_gifs = [
    "https://i.gifer.com/jU.gif",
    "https://i.gifer.com/1Dm4.gif",
    "https://i.gifer.com/fA1.gif",
    "https://i.gifer.com/XJ1C.gif",
    "https://i.gifer.com/4HZ.gif",
    "https://i.gifer.com/1XpM.gif",
    "https://i.gifer.com/7JJ.gif"
]

async def img_factory(ctx, description, url, key=None):
    try:
        bot_member = ctx.bot.cache.get_member(ctx.guild_id, ctx.bot.get_me())
        if hikari.Permissions.EMBED_LINKS in lightbulb.utils.permissions.permissions_in(
            ctx.get_channel(), bot_member
        ):
            if key:
                async with ctx.bot.d.aio_session.get(url) as command:
                    data = await command.json()
                    result = data.get(key)
            else:
                result = url
            embed = hikari.Embed(color=ctx.app.color)
            embed.set_image(result)
            await ctx.respond(description, embed=embed, reply=True, user_mentions=True)
        else:
            await ctx.respond(description, reply=True)
    except Exception as e:
        logging.error(f"Error in img_factory: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania obrazu. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo zaatakować?", hikari.User, required=False)
@lightbulb.command("anal", "*;)*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand)
@filament.utils.pass_options
async def anal(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} przypuszcza atak na {user.mention} 😉"
        await img_factory(
            ctx, desc, "https://media.tenor.com/UCbrnEEPOeQAAAAC/gay-virgin.gif"
        )
    except Exception as e:
        logging.error(f"Error in anal command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("wink", "*Puszcza oczko*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def wink(ctx: lightbulb.Context):
    try:
        desc = f"{ctx.author.mention} puszcza oczko 😉"
        await img_factory(ctx, desc, "https://some-random-api.ml/animu/wink", "link")
    except Exception as e:
        logging.error(f"Error in wink command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz uściskać?", hikari.User, required=False)
@lightbulb.command("hug", "*Przytula Cię*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def hugs(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"🤗 {ctx.author.mention} przytula {user.mention} 🤗!"
        await img_factory(ctx, desc, "https://nekos.life/api/v2/img/hug", "url")
    except Exception as e:
        logging.error(f"Error in hugs command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "W kogo chcesz się wtulić?", hikari.User, required=False)
@lightbulb.command("cuddle", "*Wtula się*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def cuddle(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"🤗 {ctx.author.mention} wtula się w {user.mention}!"
        await img_factory(ctx, desc, "https://nekos.life/api/v2/img/cuddle", "url")
    except Exception as e:
        logging.error(f"Error in cuddle command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz pocałować?", hikari.User, required=False)
@lightbulb.command(
    "kiss", "*Całuje Cię*", auto_defer=True, aliases=["buzi", "sex", "fuck"]
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def kiss(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"😘 {ctx.author.mention} całuje {user.mention}!"
        await img_factory(ctx, desc, "https://nekos.life/api/v2/img/kiss", "url")
    except Exception as e:
        logging.error(f"Error in kiss command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz uderzyć?", hikari.User, required=False)
@lightbulb.command(
    "slap", "*Uderza Cię*", auto_defer=True, aliases=["hit", "punch", "kill"]
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def slapping(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} uderza 🤜{user.mention}!"
        await img_factory(ctx, desc, "https://nekos.life/api/v2/img/slap", "url")
    except Exception as e:
        logging.error(f"Error in slapping command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz zaczepić?", hikari.User, required=False)
@lightbulb.command("poke", "*Zaczepia Cię*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def poking(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} zaczepia 👉{user.mention} 😏"
        await img_factory(ctx, desc, "http://api.nekos.fun:8080/api/poke", "image")
    except Exception as e:
        logging.error(f"Error in poking command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz połaskotać?", hikari.User, required=False)
@lightbulb.command("tickle", "*Łaskocze Cię*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def tickle(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} łaskocze 🤗{user.mention}!"
        await img_factory(ctx, desc, "http://api.nekos.fun:8080/api/tickle", "image")
    except Exception as e:
        logging.error(f"Error in tickle command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz pogłaskać?", hikari.User, required=True)
@lightbulb.command("pat", "*Głaszcze Cię*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def pats(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} głaszcze {user.mention} po głowie."
        await img_factory(ctx, desc, "https://nekos.life/api/v2/img/pat", "url")
    except Exception as e:
        logging.error(f"Error in pats command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Kogo chcesz polizać?", hikari.User, required=True)
@lightbulb.command("lick", "*Liże Cię*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def licks(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        desc = f"{ctx.author.mention} liże 👅{user.mention}😋."
        await img_factory(ctx, desc, "http://api.nekos.fun:8080/api/lick", "image")
    except Exception as e:
        logging.error(f"Error in licks command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)



@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("cry", "*Płaczę ;(*", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def cry(ctx: lightbulb.Context):
    try:
        desc = f"{ctx.author.mention} 😭"
        gif_url = random.choice(cry_gifs)
        await img_factory(ctx, desc, gif_url)
    except Exception as e:
        logging.error(f"Error in cry command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


async def comment_factory(
    ctx: lightbulb.Context,
    path: str,
    parameters: dict,
    user=None,
    scheme="https",
    host="some-random-api.ml",
):
    try:
        if isinstance(user, hikari.Member) and ctx.app.nsfw_roles & set(user.role_ids):
            parameters["avatar"] = user.default_avatar_url.url

        url = URL.build(scheme=scheme, host=host, path=path, query=parameters)
        image_data = hikari.URL(str(url))
        em = hikari.Embed(color=ctx.app.color)
        em.set_image(image_data)
        if ctx.app.embed_perms(ctx):
            await ctx.respond(embed=em, reply=True)
        else:
            await ctx.respond(str(url))
    except Exception as e:
        logging.error(f"Error in comment_factory: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania obrazu. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "message",
    "Co chcesz napisać?",
    str,
    required=True,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("member", "Imię użytkownika", hikari.Member, required=True)
@lightbulb.command("tweet", "Zatweetuj.", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def tweet(ctx: lightbulb.Context, member: hikari.Member, message: str):
    try:
        parameters = {
            "avatar": member.avatar_url.url,
            "username": member.username,
            "displayname": member.display_name or member.username,
            "comment": message,
        }
        await comment_factory(ctx, "/canvas/tweet", parameters, member)
    except Exception as e:
        logging.error(f"Error in tweet command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "message",
    "Co chcesz napisać?",
    str,
    required=True,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("member", "Imię użytkownika.", hikari.Member, required=True)
@lightbulb.command("ytcomment", "Stwórz komentarz na youtube.", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def yt(ctx: lightbulb.Context, member: hikari.Member, message: str):
    try:
        parameters = {
            "avatar": member.avatar_url.url,
            "username": member.username,
            "comment": message,
        }
        await comment_factory(ctx, "/canvas/youtube-comment", parameters, member)
    except Exception as e:
        logging.error(f"Error in yt command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Oznacz użytkownika!", hikari.User, required=True)
@lightbulb.command("comrade", "☭", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def comrade(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url)}
        await comment_factory(ctx, "/canvas/comrade", parameters, user)
    except Exception as e:
        logging.error(f"Error in comrade command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Oznacz użytkownika!", hikari.User, required=True)
@lightbulb.command("gay", "Gay-laser", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def gay(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url)}
        await comment_factory(ctx, "/canvas/gay", parameters, user)
    except Exception as e:
        logging.error(f"Error in gay command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "text",
    "Co chcesz napisać?",
    str,
    required=True,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("user", "Nazwa użytkownika!", hikari.User, required=True)
@lightbulb.command("stupid", "Głupek mem!", auto_defer=True, aliases=["sputid"])
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def stupid(ctx: lightbulb.Context, user, text):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url), "dog": text}
        await comment_factory(ctx, "/canvas/its-so-stupid", parameters, user)
    except Exception as e:
        logging.error(f"Error in stupid command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Oznacz użytkownika.", hikari.User, required=True)
@lightbulb.command("jail", "Witamy w więzieniu", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def jail(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url)}
        await comment_factory(ctx, "/canvas/jail", parameters, user)
    except Exception as e:
        logging.error(f"Error in jail command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Oznacz użytkownika.", hikari.User, required=True)
@lightbulb.command(
    "missionpass", "Zadanie wykonane. Respekt+", aliases=["pass"], auto_defer=True
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def passed(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url)}
        await comment_factory(ctx, "/canvas/passed", parameters, user)
    except Exception as e:
        logging.error(f"Error in passed command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


@img_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("user", "Oznacz użytkownika.", hikari.User, required=True)
@lightbulb.command("triggered", "TRIGGERED", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def triggered(ctx: lightbulb.Context, user):
    try:
        user = await ctx.app.get_target(ctx, user)
        parameters = {"avatar": str(user.avatar_url)}
        await comment_factory(ctx, "/canvas/triggered", parameters, user)
    except Exception as e:
        logging.error(f"Error in triggered command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)


def load(bot):
    bot.add_plugin(img_plugin)


def unload(bot):
    bot.remove_plugin(img_plugin)

