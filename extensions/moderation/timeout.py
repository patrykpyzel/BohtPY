from datetime import datetime, timedelta, timezone

import hikari
import lightbulb

timeout_plugin = lightbulb.Plugin("mute", "Mute dla użyszkodnika!")
timeout_plugin.add_checks(
    lightbulb.checks.has_guild_permissions(hikari.Permissions.MODERATE_MEMBERS),
    lightbulb.checks.bot_has_guild_permissions(hikari.Permissions.MODERATE_MEMBERS),
)


async def timeout_factory(ctx, user, second=0, minute=0, hour=0, days=0, reason=""):
    res = reason or f"{ctx.author} nie podał powodu!"

    now = datetime.now(timezone.utc)
    then = now + timedelta(days=days, hours=hour, minutes=minute, seconds=second)

    if (then - now).days > 28:
        await ctx.respond("Maksymalna ilość to 28 dni!", reply=True)
        return

    if days == 0 and hour == 0 and minute == 0 and second == 0:
        await ctx.respond(f"Usuwam muta użytkownikowi **{user}**", reply=True)
        txt = f"Mute dla {user.mention} został zdjęty!"
    else:
        await ctx.respond(f"Mutuję **{user}**!")
        txt = f"Użyszkodnik {user.mention} został zmutowany na <t:{int(then.timestamp())}:R>"
    await ctx.bot.rest.edit_member(
        user=user, guild=ctx.get_guild(), communication_disabled_until=then, reason=res
    )
    await ctx.edit_last_response(txt)


@timeout_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "Powód muta",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("days", "Ile dni muta?", int, required=False, default=0)
@lightbulb.option("hour", "Ile godzin muta?", int, required=False, default=0)
@lightbulb.option("minute", "Ile minut muta?", int, required=False, default=0)
@lightbulb.option("second", "Ile sekund muta?", int, required=False, default=0)
@lightbulb.option("user", "Użyszkodnik, którego mutujesz", hikari.Member, required=True)
@lightbulb.command(
    "mute",
    "Zmutuj użytkownika! 0 sekund = zdjęcie muta",
    auto_defer=True,
    pass_options=True,
)
@lightbulb.implements(lightbulb.SlashCommand)
async def mute(
    ctx: lightbulb.Context,
    user: hikari.Member,
    second: int,
    minute: int,
    hour: int,
    days: int,
    reason: str,
):
    await timeout_factory(ctx, user, second, minute, hour, days, reason)


@timeout_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "user", "Użyszkodnik, którego odmutowujesz", hikari.Member, required=True
)
@lightbulb.command("unmute", "Odmutuj użytkownika!", auto_defer=True, pass_options=True)
@lightbulb.implements(lightbulb.SlashCommand)
async def unmute(ctx: lightbulb.Context, user: hikari.Member):
    await timeout_factory(ctx, user)


def load(bot):
    bot.add_plugin(timeout_plugin)


def unload(bot):
    bot.remove_plugin(timeout_plugin)
