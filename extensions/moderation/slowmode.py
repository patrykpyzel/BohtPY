import hikari
import lightbulb

slowmode_plugin = lightbulb.Plugin("slowmode", "*Włączenie slowmode*")
slowmode_plugin.add_checks(
    lightbulb.checks.has_guild_permissions(hikari.Permissions.MANAGE_CHANNELS),
    lightbulb.checks.bot_has_guild_permissions(hikari.Permissions.MANAGE_CHANNELS),
)


@slowmode_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "interval",
    "Ile sekund slowmode? 0 dla wyłączenia.",
    int,
    min_value=0,
    max_value=21600,
    required=False,
)
@lightbulb.option(
    "channel", "Na którym kanale?", hikari.TextableGuildChannel, required=True
)
@lightbulb.command(
    "slowmode", "Ustaw slowmode na kanale", auto_defer=True, pass_options=True
)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def slowmode(
    ctx: lightbulb.Context, channel: hikari.TextableGuildChannel, interval: int
):
    time = interval or 0
    if time == 0:
        await ctx.respond(f"Usuwam slowmode z kanału", reply=True)
    else:
        await ctx.respond(
            f"Ustawiam slowmode **{time} sekund na wybranym kanlale**", reply=True
        )
    await ctx.bot.rest.edit_channel(channel, rate_limit_per_user=time)


def load(bot):
    bot.add_plugin(slowmode_plugin)


def unload(bot):
    bot.remove_plugin(slowmode_plugin)
