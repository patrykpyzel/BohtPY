import asyncio
from datetime import datetime, timedelta, timezone
import hikari
import lightbulb
import miru

purge_plugin = lightbulb.Plugin("purge", "Usuń czat!")

async def purge_messages(
    ctx: lightbulb.Context, amount: int, channel: hikari.Snowflakeish
) -> None:
    iterator = (
        ctx.bot.rest.fetch_messages(channel)
        .limit(amount)
        .take_while(
            lambda msg: (datetime.now(timezone.utc) - msg.created_at)
            < timedelta(days=14)
        )
    )
    if iterator:
        async for messages in iterator.chunk(100):
            await ctx.bot.rest.delete_messages(channel, messages)
        await ctx.respond(f"**Wiadomości usunięte.**")
    else:
        await ctx.respond("Nie znalazłem żadnych wiadomości!")

class PromptView(miru.View):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.accepted = None

    async def view_check(self, ctx: miru.ViewContext) -> bool:
        return ctx.user.id == ctx.interaction.user.id

class ConfirmButton(miru.Button):
    def __init__(self) -> None:
        super().__init__(style=hikari.ButtonStyle.DANGER, label="Potwierdź")

    async def callback(self, ctx: miru.ViewContext) -> None:
        self.view.accepted = True
        await self.view.stop()

class CancelButton(miru.Button):
    def __init__(self) -> None:
        super().__init__(style=hikari.ButtonStyle.SUCCESS, label="Odrzuć")

    async def callback(self, ctx: miru.ViewContext) -> None:
        self.view.accepted = False
        await self.view.stop()

@purge_plugin.command
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.add_checks(
    lightbulb.has_guild_permissions(hikari.Permissions.MANAGE_MESSAGES),
    lightbulb.bot_has_guild_permissions(hikari.Permissions.MANAGE_MESSAGES),
    lightbulb.guild_only,
)
@lightbulb.option(
    "amount", "The number of messages to purge.", type=int, required=True, max_value=500
)
@lightbulb.command(
    "purge",
    "Usuń wiadomości z tego kanału.",
    aliases=["clear", "prune"],
    auto_defer=True,
    pass_options=True,
)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def purge(ctx: lightbulb.Context, amount: int) -> None:
    if amount > 500:
        await ctx.respond(":x: **Maksymalnie 500 wiadomości!**")
        return

    channel = ctx.channel_id
    if isinstance(ctx, lightbulb.PrefixContext):
        await ctx.event.message.delete()

    pruneview = PromptView(timeout=30)
    pruneview.add_item(ConfirmButton())
    pruneview.add_item(CancelButton())

    prompt = await ctx.respond(
        "Jesteś pewien, że chcesz kontynuować? **Akcja nieodwracalna!**",
        components=pruneview.build(),
        flags=hikari.MessageFlag.EPHEMERAL,
    )
    msg = await prompt.message()
    await pruneview.start(msg)
    await pruneview.wait()

    if pruneview.accepted is True:
        await purge_messages(ctx, amount, channel)
    elif pruneview.accepted is False:
        await ctx.respond(f"**Odrzucono!**", delete_after=7)
    else:
        await ctx.respond(f"**Odrzucono z powodu nieaktywności!**", delete_after=7)

def load(bot: lightbulb.BotApp) -> None:
    bot.add_plugin(purge_plugin)

def unload(bot: lightbulb.BotApp) -> None:
    bot.remove_plugin(purge_plugin)