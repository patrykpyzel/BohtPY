from datetime import datetime
from typing import Union

import hikari
import lightbulb
from lightbulb.ext import filament

test_plugin = lightbulb.Plugin("Test", "Testowe komendy")


@test_plugin.command
@lightbulb.option("target", "test member", hikari.Member, required=False)
@lightbulb.command(
    "test", "Sprawdź avatar użytkownika.", auto_defer=True, ephemeral=False
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def test_member(ctx: lightbulb.Context, target: hikari.Member):
    pass


def load(bot) -> None:
    bot.add_plugin(test_plugin)


def unload(bot) -> None:
    bot.remove_plugin(test_plugin)
