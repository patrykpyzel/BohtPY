import hikari
import lightbulb
from lightbulb.utils import nav, pag
import logging
import traceback

ban_plugin = lightbulb.Plugin("ban", "Prepare the ban hammer!! (Please use it wisely")

ban_plugin.add_checks(
    lightbulb.checks.has_guild_permissions(hikari.Permissions.BAN_MEMBERS),
    lightbulb.checks.bot_has_guild_permissions(hikari.Permissions.BAN_MEMBERS),
    lightbulb.guild_only,
)

@ban_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "Powód bana.",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option(
    "delete_message",
    "Liczba dni, z których usunąć wiadomości użytkownika po banie. Maksymalnie 7 dni. Domyślnie 1",
    int,
    min_value=0,
    max_value=7,
    default=1,
    required=False,
)
@lightbulb.option("user", "Użytkownik, którego banujesz", hikari.User, required=False)
@lightbulb.command("ban", "Zbanuj użytkownika", auto_defer=True, pass_options=True)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def ban(
    ctx: lightbulb.Context, user: hikari.User = None, delete_message: int = 1, reason: str = None
):
    try:
        if not ctx.guild_id:
            return await ctx.respond("Ta komenda działa tylko na serwerach.", reply=True)
        
        target = await ctx.app.get_target(ctx, user)
        
        if target is None:
            return await ctx.respond("Nie znaleziono użytkownika do zbanowania.", reply=True)
        
        if target.id == ctx.author.id:
            return await ctx.respond("Nie możesz zbanować samego siebie!", reply=True)
        
        if target.id == ctx.bot.get_me().id:
            return await ctx.respond("Nie mogę zbanować samego siebie!", reply=True)

        author_permissions = lightbulb.utils.permissions_for(ctx.member)
        if hikari.Permissions.BAN_MEMBERS not in author_permissions:
            return await ctx.respond("Nie masz uprawnień do banowania użytkowników.", reply=True)
        
        bot_member = ctx.bot.cache.get_member(ctx.guild_id, ctx.bot.get_me())
        bot_permissions = lightbulb.utils.permissions_for(bot_member)
        if hikari.Permissions.BAN_MEMBERS not in bot_permissions:
            return await ctx.respond("Nie mam uprawnień do banowania użytkowników.", reply=True)

        res = f"{ctx.author.username} {ctx.author.id} - {reason or 'Nie podano powodu'}"
        await ctx.respond(f"Banuję! **{target.username}**", reply=True)
        
        # Konwertujemy dni na sekundy
        delete_message_seconds = delete_message * 24 * 60 * 60
        
        await ctx.bot.rest.ban_member(
            user=target,
            guild=ctx.get_guild(),
            reason=res,
            delete_message_seconds=delete_message_seconds
        )
        await ctx.edit_last_response(f"Zbanowałem `{target}`!")
    except hikari.ForbiddenError:
        await ctx.respond("Nie mam wystarczających uprawnień, aby zbanować tego użytkownika.", reply=True)
    except Exception as e:
        logging.error(f"Error in ban command: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas wykonywania komendy.", reply=True)



@ban_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "Powód odbanowania.",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option(
    "user", "Kogo chcesz odbanować? (Podaj ID)", hikari.Snowflake, required=True
)
@lightbulb.command("unban", "Odbanuj użytkownika.", auto_defer=True, pass_options=True)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def unban(ctx: lightbulb.Context, user: hikari.Snowflake, reason: str):
    res = reason or f"'Brak powodu' - {ctx.author.username}"
    await ctx.respond(f"Odbanowanie ID **{user}**")
    await ctx.bot.rest.unban_member(user=user, guild=ctx.get_guild(), reason=res)
    await ctx.edit_last_response(f"Odbanowałem`{user}`! `{res}`!")


@ban_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("banlist", "Lista zbanowanych osób na serwerze!", auto_defer=True)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def banlist(ctx: lightbulb.Context):
    bans = await ctx.bot.rest.fetch_bans(ctx.get_guild())
    lst = pag.EmbedPaginator()

    @lst.embed_factory()
    def build_embed(page_index, page_content):
        emb = hikari.Embed(title="List of Banned Members", description=page_content)
        emb.set_footer(f"{len(bans)} zbanowanych.")
        return emb

    for n, users in enumerate(bans, start=1):
        lst.add_line(f"**{n}. {users.user}** ({users.reason or 'No Reason Provided.'})")
    navigator = nav.ButtonNavigator(lst.build_pages())
    await navigator.run(ctx)


def load(bot):
    bot.add_plugin(ban_plugin)


def unload(bot):
    bot.remove_plugin(ban_plugin)
