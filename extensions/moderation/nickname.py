import hikari
import lightbulb

nickname_plugin = lightbulb.Plugin("nickname", "*Jaki nick tym razem?*")
nickname_plugin.add_checks(
    lightbulb.checks.has_guild_permissions(hikari.Permissions.CHANGE_NICKNAME),
    lightbulb.checks.bot_has_guild_permissions(hikari.Permissions.CHANGE_NICKNAME),
    lightbulb.guild_only,
)


@nickname_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "nick", "Nowy nick na serwerze. Zostaw puste by zresetować!", str, required=False
)
@lightbulb.option("member", "Komu chcesz zmienić nick?", hikari.Member, required=True)
@lightbulb.command(
    "nickname", "Zmień nick na serwerze.", auto_defer=True, pass_options=True
)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def nickname(ctx: lightbulb.Context, member: hikari.Member, nick: str):
    can_change_others = (
        hikari.Permissions.MANAGE_NICKNAMES
        in lightbulb.utils.permissions_for(ctx.member)
    )
    if member.id != ctx.member.id and not can_change_others:
        return
    await ctx.bot.rest.edit_member(ctx.guild_id, user=member, nickname=nick)
    await ctx.respond(f"Zmieniłem {member.mention} nick na {nick}!", reply=True)


def load(bot):
    bot.add_plugin(nickname_plugin)


def unload(bot):
    bot.remove_plugin(nickname_plugin)
