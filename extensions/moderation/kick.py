import hikari
import lightbulb

kick_plugin = lightbulb.Plugin("kick", "Wyrzuć użytkownika")
kick_plugin.add_checks(
    lightbulb.checks.has_guild_permissions(hikari.Permissions.KICK_MEMBERS),
    lightbulb.checks.bot_has_guild_permissions(hikari.Permissions.KICK_MEMBERS),
    lightbulb.guild_only,
)


@kick_plugin.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option(
    "reason",
    "Powód kicka.",
    str,
    required=False,
    modifier=lightbulb.commands.OptionModifier.CONSUME_REST,
)
@lightbulb.option("user", "Kogo chcesz wyrzucić?", hikari.User, required=True)
@lightbulb.command("kick", "Wyrzuć użyszkodnika", auto_defer=True, pass_options=True)
@lightbulb.implements(lightbulb.SlashCommand, lightbulb.PrefixCommand)
async def kick(ctx: lightbulb.Context, user, reason):
    res = reason or f"'Nie podano powodu' - {ctx.author.username}"
    await ctx.respond(f"Wyrzucam **{user}**")
    await ctx.bot.rest.kick_member(user=user, guild=ctx.get_guild(), reason=res)
    await ctx.edit_last_response(f"Wyrzuciłem `{user}`!")


def load(bot):
    bot.add_plugin(kick_plugin)


def unload(bot):
    bot.remove_plugin(kick_plugin)
