from datetime import datetime
from random import randint

import hikari
import lightbulb
from lightbulb.ext import tasks

from utils import database

db = database.ORM()


message_events = lightbulb.Plugin("Message", "Message events", include_datastore=True)


@message_events.listener(hikari.GuildMessageCreateEvent)
async def on_guild_message_create(event: hikari.GuildMessageCreateEvent):
    msg = event.message
    content = msg.content
    if msg.author.is_bot or content is None or len(content.split()) < 4:
        return
    guild_id = msg.guild_id
    author_id = msg.author.id
    if 960665311760248877 in event.member.role_ids:
        if randint(0, 9) != 0:
            return
    if isinstance(
        await event.app.rest.fetch_channel(msg.channel_id),
        hikari.channels.GuildVoiceChannel,
    ):
        return

    await db.add_points(guild_id, author_id)


def load(bot) -> None:
    bot.add_plugin(message_events)


def unload(bot) -> None:
    message_events.d.clear()
    bot.remove_plugin(message_events)
