import logging
from datetime import datetime, timedelta

import hikari
import lightbulb
from lightbulb.ext import tasks

from utils import database

db = database.ORM()

member_events = lightbulb.Plugin("Member", "Member events", include_datastore=True)


@member_events.listener(hikari.MemberCreateEvent)
async def on_member_create(event: hikari.MemberCreateEvent):
    guild_id = event.guild_id
    member = event.member

    await db.add_points(guild_id, member.id)

    logging.info(f"[MEMBER ADD] {member} {member.id} {guild_id})")

    zagadka_member = event.app.zagadka_guild.get_member(member)
    if zagadka_member is None:
        if datetime.now() - event.app.last_message_time < timedelta(seconds=4):
            return
        else:
            try:
                await member.send(
                    f"""**Cześć, chcesz zwiększyć aktywność serwera tak by urósł jak największy polski serwer zaGadka?**
    Lub może znasz kogoś kto chce?
    Wystarczy, dodać mnie serwer a ja będę liczył waszą aktywność, reputację i wiele innych!
    **Tutaj możesz mnie dodać:**
    {event.app.invite_bot_url}
    **Wspomniany najaktywniejszy serwer w PL:**
    https://discord.gg/tUzZDd32jZ"""
                )
                event.app.last_message_time = datetime.now()
            except (hikari.errors.ForbiddenError, hikari.errors.BadRequestError):
                pass


@tasks.task(h=1, auto_start=True)
async def refresh_zagadka_members():
    if member_events.app.d.zagadka_members is not None:
        await member_events.app.refresh_zagadka()


def load(bot) -> None:
    bot.add_plugin(member_events)


def unload(bot) -> None:
    # member_events.d.clear()
    bot.remove_plugin(member_events)
