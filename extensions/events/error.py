import logging
from typing import Union

import hikari
import lightbulb

error_events = lightbulb.Plugin("Error", "Error events", include_datastore=False)

async def check_instances(e, ctx):
    if type(e) is lightbulb.CheckFailure:
        return await check_instances(e.causes[0], ctx)
    elif isinstance(e, lightbulb.CommandNotFound):
        return True
    elif isinstance(e, lightbulb.MissingRequiredPermission):
        return await ctx.respond("Nie masz permisji!", reply=True)
    elif isinstance(e, lightbulb.BotMissingRequiredPermission):
        await ctx.respond("Nie mam wystarczających permisji :(", reply=True)
        raise e
    elif isinstance(e, lightbulb.errors.CommandIsOnCooldown):
        return await ctx.respond("Zwolnij koleżko!", reply=True)
    elif isinstance(e, lightbulb.errors.NotEnoughArguments):
        return await ctx.respond("Dodaj argumenty do komendy!", reply=True)
    elif isinstance(e, lightbulb.CommandInvocationError):
        logging.error(f"Command invocation error: {e}", exc_info=True)
        await ctx.respond("Nie mam uprawnień ;(", reply=True)
        raise e

@error_events.listener(lightbulb.CommandErrorEvent)
async def on_error(event: lightbulb.CommandErrorEvent) -> None:
    if await check_instances(event.exception, event.context):
        return
    logging.error(f"Unexpected error: {event.exception}", exc_info=True)
    raise event.exception

def load(bot) -> None:
    bot.add_plugin(error_events)

def unload(bot) -> None:
    bot.remove_plugin(error_events)
