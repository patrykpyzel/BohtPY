import logging
from datetime import datetime

import hikari
import lightbulb
from lightbulb.ext import tasks

from utils import database

db = database.ORM()


guild_events = lightbulb.Plugin("Guild", "Guild events", include_datastore=True)


@guild_events.listener(hikari.GuildJoinEvent)
async def on_guild_join(event: hikari.GuildMessageCreateEvent):
    guild = event.guild
    owner = await guild.fetch_owner()

    await db.set_guild(event.guild_id)
    logging.info(
        f"[GUILD JOIN] {guild.name} ({guild.id}) added the bot. Owner: {owner} ({owner.id})`"
    )


def load(bot) -> None:
    bot.add_plugin(guild_events)


def unload(bot) -> None:
    guild_events.d.clear()
    bot.remove_plugin(guild_events)
