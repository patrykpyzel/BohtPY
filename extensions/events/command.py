import logging

import hikari
import lightbulb

command_events = lightbulb.Plugin("Command", "Command events", include_datastore=False)


@command_events.listener(lightbulb.CommandInvocationEvent)
async def on_command(event: lightbulb.CommandInvocationEvent) -> None:

    logging.info(
        f"[COMMAND RUN] {event.command.name} | "
        f"Author:{event.context.author} "
        f"({event.context.author.id}) "
        f'Guild: {event.context.get_guild().name[:30] if event.context.get_guild() else "DM"}'
    )


def load(bot) -> None:
    bot.add_plugin(command_events)


def unload(bot) -> None:
    bot.remove_plugin(command_events)
