from datetime import datetime

import hikari
import lightbulb
from lightbulb.ext import tasks

info = lightbulb.Plugin("info", "informacje o bocie", include_datastore=True)


@info.command()
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.command("invite", "Zaproś bota na serwer!", aliases=["i", "inv"])
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def invite(ctx: lightbulb.Context):
    await ctx.respond(ctx.app.invite_bot_url)


def load(bot) -> None:
    bot.add_plugin(info)


def unload(bot) -> None:
    bot.remove_plugin(info)
