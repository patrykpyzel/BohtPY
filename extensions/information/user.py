import logging
from datetime import datetime
from typing import Union

import hikari
import lightbulb
from lightbulb.ext import filament
import traceback

user_plugin = lightbulb.Plugin("user", "Komendy Użytkownika")

# Domyślny kolor embeda
DEFAULT_EMBED_COLOR = 0x3B9DFF

def get_embed_color(user: Union[hikari.User, hikari.Member], ctx: lightbulb.Context) -> int:
    """Zwraca kolor embeda zgodny z kolorem najwyższej roli użytkownika."""
    if isinstance(user, hikari.Member):
        roles = [ctx.app.cache.get_role(role_id) for role_id in user.role_ids if ctx.app.cache.get_role(role_id)]
        sorted_roles = sorted(roles, key=lambda r: r.position, reverse=True)
        for role in sorted_roles:
            if role.color:
                return role.color
    return user.accent_color or DEFAULT_EMBED_COLOR

async def check_nsfw(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User]) -> bool:
    """Sprawdza czy użytkownik ma rangę NSFW."""
    if isinstance(target, hikari.Member) and ctx.app.nsfw_roles & set(target.role_ids):
        return True
    return False

async def check_permissions(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User], permission: hikari.Permissions) -> bool:
    """Sprawdza czy użytkownik ma odpowiednie uprawnienia."""
    if ctx.guild_id:
        member = ctx.member
        if member is None:
            member = await ctx.bot.rest.fetch_member(ctx.guild_id, ctx.author.id)
        
        if not isinstance(target, hikari.Member):
            perms = lightbulb.utils.permissions_for(member)
            return bool(perms & permission)
    return True

@user_plugin.command
@lightbulb.option(
    "target",
    "Czyje informacje chcesz wyświetlić? Oznacz lub podaj ID",
    hikari.User,
    required=False,
)
@lightbulb.command(
    "profile",
    "Informacje o użytkowniku. Nawet jak nie ma go na serwerze!",
    aliases=["userinfo", "info", "p", "profil"],
    ephemeral=False,
    auto_defer=False,
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_profile(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User, int, str] = None) -> None:
    try:
        logging.info(f"Running profile command by {ctx.author.id} in guild {ctx.guild_id}")
        target = await ctx.app.get_target(ctx, target)
        if not target:
            return await ctx.respond("Nie znaleziono użytkownika!")

        logging.info(f"Target acquired: {target.id}")
        logging.info(f"Target type: {type(target)}")

        nsfw = await check_nsfw(ctx, target)

        if not isinstance(target, hikari.Member) and not await check_permissions(ctx, target, hikari.Permissions.MANAGE_MESSAGES):
            return await ctx.respond("Nie masz uprawnień do wyświetlania profilu użytkownika spoza serwera!")

        role_ids = set()
        vc = None

        if isinstance(target, hikari.Member):
            role_ids = {role for role in target.role_ids if role != ctx.guild_id}
            vc = ctx.bot.cache.get_voice_state(ctx.guild_id, target)

        member_db = await ctx.app.d.db.get_member(target.id, ctx.guild_id) if ctx.guild_id else None

        created_at = int(target.created_at.timestamp())
        joined_at = int(target.joined_at.timestamp()) if isinstance(target, hikari.Member) else None

        user = await ctx.bot.rest.fetch_user(target.id)
        
        banner_url = user.banner_url
        if not banner_url and isinstance(target, hikari.Member):
            banner_url = target.guild_avatar_url

        logging.info(f"Banner URL: {banner_url}")

        embed_color = get_embed_color(target, ctx)

        emb = hikari.Embed(
            title=f"Profil - {target}",
            description=f"ID: `{target.id}`",
            colour=embed_color,
            timestamp=datetime.now().astimezone(),
        )

        if not nsfw:
            emb.set_thumbnail(target.avatar_url or target.default_avatar_url)
            if banner_url:
                emb.set_image(banner_url)
                emb.add_field("Banner URL", f"[Kliknij tutaj]({banner_url})", inline=False)
                logging.info("Banner added to embed")
            else:
                logging.info("Banner not added to embed")
        else:
            logging.info("Avatar and Banner not displayed due to NSFW status")

        emb.add_field(
            "Konto stworzone",
            f"<t:{created_at}:d>\n(<t:{created_at}:R>)",
            inline=True,
        )
        if joined_at:
            emb.add_field(
                "Konto dołączyło",
                f"<t:{joined_at}:d>\n(<t:{joined_at}:R>)",
                inline=True,
            )
        if role_ids:
            emb.add_field(
                "Role",
                ", ".join(f"<@&{r}>" for r in role_ids) or "Brak ról.",
                inline=False,
            )
        if member_db:
            if member_db.reputation:
                emb.add_field("Reputacja", str(member_db.reputation), inline=True)
            if member_db.score:
                emb.add_field("Punkty", str(member_db.score), inline=True)
            if member_db.temp_score:
                emb.add_field("Punkty m", str(member_db.temp_score), inline=True)

        emb.add_field(name="Oznaczenie", value=target.mention, inline=False)
        if vc:
            emb.add_field(name="VC", value=f"<#{vc.channel_id}>", inline=True)

        await ctx.respond(embed=emb if ctx.app.embed_perms(ctx) else None, content=str(emb) if not ctx.app.embed_perms(ctx) else None, reply=True)

    except Exception as e:
        logging.error(f"Error in user_profile: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@user_plugin.command
@lightbulb.add_cooldown(3, 3, lightbulb.UserBucket)
@lightbulb.option("target", "Czyj banner wyświetlić?", hikari.User, required=False)
@lightbulb.command("banner", "Wyświetl banner użytkownika", auto_defer=True, ephemeral=False, aliases=["b"])
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_banner(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User, int, str] = None):
    try:
        logging.info(f"Running banner command by {ctx.author.id} in guild {ctx.guild_id}")

        target = await ctx.app.get_target(ctx, target)
        if not target:
            return await ctx.respond("Nie znaleziono użytkownika!")

        logging.info(f"Target acquired: {target.id}")

        nsfw = await check_nsfw(ctx, target)
        if nsfw:
            return await ctx.respond(embed=hikari.Embed(description="Ktoś tu ma muta ;)"))

        if not isinstance(target, hikari.Member) and not await check_permissions(ctx, target, hikari.Permissions.MANAGE_MESSAGES):
            return await ctx.respond("Nie masz uprawnień do wyświetlania bannera użytkownika spoza serwera!")

        user = await ctx.bot.rest.fetch_user(target.id)
        
        banner_url = user.banner_url
        if not banner_url and isinstance(target, hikari.Member):
            banner_url = target.guild_avatar_url

        if not banner_url:
            return await ctx.respond(embed=hikari.Embed(description=f"**{user.username}** nie ma bannera!"))

        embed_color = get_embed_color(target, ctx)

        bnr = hikari.Embed(
            description=f"**{user.username}**'s Banner",
            title="Banner Viewer",
            color=embed_color,
            timestamp=datetime.now().astimezone(),
        )
        bnr.set_image(banner_url)

        await ctx.respond(embed=bnr if ctx.app.embed_perms(ctx) else None, content=str(banner_url) if not ctx.app.embed_perms(ctx) else None, reply=True)

    except Exception as e:
        logging.error(f"Error in user_banner: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@user_plugin.command
@lightbulb.option("server", "Czy chcesz sprawdzić avatar dla tego serwera?", bool, required=False, default=False)
@lightbulb.option("target", "Czyj avatar wyświetlić?", hikari.User, required=False)
@lightbulb.command("avatar", "Sprawdź avatar użytkownika.", auto_defer=True, aliases=["a", "av", "pfp", "ava"], ephemeral=False)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_avatar(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User, int, str] = None, server: bool = False):
    try:
        logging.info(f"Running avatar command by {ctx.author.id} in guild {ctx.guild_id}")

        target = await ctx.app.get_target(ctx, target)
        if not target:
            return await ctx.respond("Nie znaleziono użytkownika!")

        logging.info(f"Target acquired: {target.id}")

        nsfw = await check_nsfw(ctx, target)
        if nsfw:
            return await ctx.respond(embed=hikari.Embed(description="Ktoś tu ma muta ;)"))

        if not isinstance(target, hikari.Member) and not await check_permissions(ctx, target, hikari.Permissions.MANAGE_MESSAGES):
            return await ctx.respond("Nie masz uprawnień do wyświetlania avatara użytkownika spoza serwera!")

        pfp = target.guild_avatar_url if isinstance(target, hikari.Member) and target.guild_avatar_url and server else target.avatar_url or target.default_avatar_url

        embed_color = get_embed_color(target, ctx)

        ava = hikari.Embed(
            title=f"{target}",
            color=embed_color,
            timestamp=datetime.now().astimezone(),
            description=f"[avatar url]({pfp}) || [invite bot]({ctx.bot.invite_bot_url})",
        )
        ava.set_image(pfp)

        await ctx.respond(embed=ava if ctx.app.embed_perms(ctx) else None, content=str(pfp) if not ctx.app.embed_perms(ctx) else None, reply=True)

    except Exception as e:
        logging.error(f"Error in user_avatar: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@user_plugin.command
@lightbulb.option("target", "Czyje ID chcesz sprawdzić?", hikari.Member, required=False)
@lightbulb.command("id", "Sprawdź ID użytkownika.", auto_defer=True, ephemeral=False)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_id(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User, int, str] = None):
    try:
        target = await ctx.app.get_target(ctx, target)
        if not target:
            return await ctx.respond("Nie znaleziono użytkownika!")
        await ctx.respond(str(target.id), reply=True)
    except Exception as e:
        logging.error(f"Error in user_id: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

@user_plugin.command
@lightbulb.option("target", "Czyj kanał głosowy chcesz zobaczyć?", hikari.Member, required=False)
@lightbulb.command("vc", "Sprawdź kanał głosowy na którym jest użytkownik", auto_defer=True, ephemeral=False)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
@filament.utils.pass_options
async def user_vc(ctx: lightbulb.Context, target: Union[hikari.Member, hikari.User, int, str] = None):
    try:
        target = await ctx.app.get_target(ctx, target)
        if not target:
            return await ctx.respond("Nie znaleziono użytkownika!")

        if not isinstance(target, hikari.Member):
            return await ctx.respond("Ten użytkownik nie jest członkiem tego serwera!")

        vc = ctx.bot.cache.get_voice_state(ctx.guild_id, target)

        if vc:
            await ctx.respond(f"<#{vc.channel_id}>", reply=True)
        else:
            await ctx.respond("Ten użytkownik nie znajduje się na kanale głosowym!", reply=True)
    except Exception as e:
        logging.error(f"Error in user_vc: {str(e)}")
        logging.error(traceback.format_exc())
        await ctx.respond("Wystąpił błąd podczas przetwarzania komendy. Spróbuj ponownie później.", reply=True)

def load(bot) -> None:
    bot.add_plugin(user_plugin)

def unload(bot) -> None:
    bot.remove_plugin(user_plugin)
