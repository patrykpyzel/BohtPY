import asyncio
import logging
import re
from datetime import datetime, timedelta, timezone

import hikari
import lightbulb

info_plugin = lightbulb.Plugin("Remind")

invite_url = "https://discord.gg/zagadka"


@info_plugin.command
@lightbulb.add_cooldown(30, 2, lightbulb.UserBucket)
@lightbulb.option(
    "text", "Co przypomnieć?", str, required=False, default="Przypomnienie!"
)
@lightbulb.option("hour", "Za ile godzin przypomnieć?", int, required=False, default=0)
@lightbulb.option("minute", "Za ile minut przypomnieć?", int, required=False, default=0)
@lightbulb.option(
    "second", "Za ile sekund przypomnieć?", int, required=False, default=0
)
@lightbulb.command(
    "remind",
    "Ustaw przypominajkę.",
    auto_defer=True,
    pass_options=True,
    aliases=["rem"],
)
@lightbulb.implements(lightbulb.PrefixCommand, lightbulb.SlashCommand)
async def remind(
    ctx: lightbulb.Context,
    second: int = 0,
    minute: int = 0,
    hour: int = 0,
    text: str = "Przypomnienie!",
) -> None:
    if isinstance(ctx, lightbulb.PrefixContext):
        if second:
            minute = second
            second = 0
        else:
            try:
                _, t, symbol, *_ = re.split(r"(\d+)", text)

                if not symbol or symbol.lower().startswith("m"):
                    minute = int(t)
                elif symbol.lower().startswith("s"):
                    second = int(t)
                elif symbol.lower().startswith("h"):
                    hour = int(t)
            except ValueError as e:
                logging.error(e)
                return await ctx.respond(
                    "Oj coś poszło nie tak. Przykładowa komenda: `.rem 12m`", reply=True
                )
            else:
                text = "Przypomnienie!"

    seconds_all = (hour * 60 * 60) + (minute * 60) + second

    now = datetime.now(timezone.utc)
    then = now + timedelta(hours=hour, minutes=minute, seconds=second)

    if seconds_all == 0:
        return await ctx.respond(
            "Oj coś poszło nie tak. Przykładowa komenda: `.rem 12m`", reply=True
        )

    await ctx.respond(f"Przypomnę <t:{int(then.timestamp())}:R>", reply=True)
    await asyncio.sleep(seconds_all)

    await ctx.respond(f"{ctx.author.mention} {text}", reply=True, user_mentions=True)


def load(bot: lightbulb.BotApp) -> None:
    bot.add_plugin(info_plugin)
